use async_std::task;
use rodio::Sink;
use shared_types::SamplesBuffer;
use shared_types::TimeTaggedSamplesBufferI16;
use std::collections::BinaryHeap;
use std::io::{Read, Write};
use std::net::{Shutdown, TcpListener, TcpStream};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::SystemTime;

struct State {
    //O "poppable element" é o próximo a ser tocado
    pub audio_packets: BinaryHeap<TimeTaggedSamplesBufferI16>,
    pub player: Sink,
}

fn process_audio_stream(stream: &TcpStream, state: Arc<Mutex<State>>) {
    loop {
        /*let mut data = vec![];
        let n = stream.read_to_end(&mut data).unwrap();*/
        let packet = bincode::deserialize_from(stream);
        let packet: TimeTaggedSamplesBufferI16 = match packet {
            Ok(packet) => packet,
            Err(_e) => continue,
        };

        let mut state = state.lock().expect("sassa");
        state.audio_packets.push(packet);
    }
}

fn process_time_stream(stream: &TcpStream, state: Arc<Mutex<State>>) {
    loop {
        //let mut data = vec![];
        //let n = stream.read_to_end(&mut data).expect("smeyhn");

        //let packet: SystemTime = bincode::deserialize(&data[..n]).expect("smeyhn");
        let packet = bincode::deserialize_from(stream);
        let packet: SystemTime = match packet {
            Ok(packet) => packet,
            Err(_e) => continue,
        };
        let mut state = state.lock().expect("smessssssssssssssssssyhn");
        let first_packet = state.audio_packets.peek();
        let first_packet = match first_packet {
            Some(first_packet) => first_packet,
            None => continue,
        };
        let start_time = first_packet.start_time;
        if packet > start_time {
            let device = rodio::default_output_device().unwrap();
            let player = Sink::new(&device);
            player.append(state.audio_packets.pop().unwrap().source);
            state.player = player;
            state.player.play();
        }
    }
}

fn main() {
    let audio_packets = BinaryHeap::new();
    let device = rodio::default_output_device().unwrap();
    let player = Sink::new(&device);
    let state = State {
        audio_packets,
        player,
    };
    let state = Arc::new(Mutex::new(state));
    let state_ref = state.clone();
    let addr = "0.0.0.0";
    let _audio_thread = thread::Builder::new()
        .name("audio".to_string())
        .spawn(move || {
            let port = "4000";
            let addr = format!("{}:{}", addr, port);
            let listener = TcpListener::bind(addr).unwrap();
            // accept connections and process them, spawning a new thread for each one
            println!("Server listening on port 4000");
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        println!("New connection: {}", stream.peer_addr().unwrap());
                        let state_ref = state_ref.clone();
                        process_audio_stream(&stream, state_ref);
                    }
                    Err(e) => {
                        println!("Error: {}", e);
                        /* connection failed */
                    }
                }
            }
            // close the socket server (redundant)
            //drop(listener);
        });

    let state_ref = state.clone();
    let _time_thread = thread::Builder::new()
        .name("time".to_string())
        .spawn(move || {
            let port = "4001";
            let addr = format!("{}:{}", addr, port);
            let listener = TcpListener::bind(addr).unwrap();
            // accept connections and process them, spawning a new thread for each one
            println!("Server listening on port 4001");
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        println!("New connection: {}", stream.peer_addr().unwrap());
                        let state_ref = state_ref.clone();
                        process_time_stream(&stream, state_ref);
                    }
                    Err(e) => {
                        println!("Error: {}", e);
                        /* connection failed */
                    }
                }
                println!("se2");
            }
            println!("saiu2");
            // close the socket server (redundant)
            //drop(listener);
        });
    thread::park();
}
